﻿using UnityEngine;
using System.Collections;
namespace TwoDStyle
{
    [RequireComponent(typeof(Camera))]
    public class CamerasCallToRenderer : MonoBehaviour
    {

        private Transform thisCamTransform;
        // Use this for initialization
        void Awake()
        {
            thisCamTransform = this.GetComponent<Transform>();
        }

        public delegate void PreCullEvent(Transform camera);
        public static PreCullEvent onPreCull;

        void OnPreCull()
        {
            if (onPreCull != null)
            {
                onPreCull(thisCamTransform);
            }
        }

    }
}
